# starsNblackdots
This script should help you pray. </br>

Remember back in school when for every good action the teacher would give you a star? and then for every bad thing you do the teacher would give you a black dot. </br>

huh! That's just kid's stuff right? <big>WRONG.</big> Turns out this is a very effective strategy for everybody. </br>

Whether you do not pray regularly and you would like to, or maybe you do pray regularly but you have issues concentrating through the whole prayer. </br>

The idea of this little script is simple. </br>

At the end of the day you give it a report for how your day went. I prayed 3 prayers and missed 2. you just enter 3 and it will give you three stars and 2 black dots. and then it will show you a graph on where you stand relative to your average over time. </br>

