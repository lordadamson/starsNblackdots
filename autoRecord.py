import os
import sys

#if you give this method a string that is composed of several lines, it returns a list where each element is a line from the given string
def tokenizer(your_str):
	out = []
	buff = []
	for c in your_str:
	    if c == '\n':
		out.append(''.join(buff))
		buff = []
	    else:
		buff.append(c)
	else:
	    if buff:
	       out.append(''.join(buff))

	return out

def stars_to_percentage(string):
	return str(int((float(score)/5)*100))

#score is the first argument you pass to the script from the command line
score = str(sys.argv[1])
score = stars_to_percentage(score)

#opens record at read write mode.
file = open('record', 'r')

record = file.read()

output = tokenizer(record)

#incriments number of days
numDays = int(output[0])
numDays = numDays + 1
output[0] = str(numDays)

#adds today's score to the total score
total = int(output[1])
total = total + int(score)
output[1] = str(total)

#adds today's score to the end of the list
output.append(score)

#turn the output list to a string
record = '\n'.join(output)

#print out average score
print (float(output[1])/float(output[0]))

#write output to the file
tempFile = open("tempFile", "w")
tempFile.write(record)
os.rename('tempFile', 'record')

#obviosly closing the files...
file.close()
tempFile.close()
